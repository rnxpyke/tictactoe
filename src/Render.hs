{-# LANGUAGE TypeSynonymInstances, FlexibleInstances #-}

module Render 
    ( render
    , Cell (..)
    ) where 

import Game
import Data.Array
import Graphics.Gloss
import Grid


render :: Area -> Game -> Picture
render a g = draw a (Grid $ board g)

renderCell :: Cell -> Picture
renderCell = norm

instance Drawable Cell where
    norm Nothing  = blank
    norm (Just O) = circle 0.8
    norm (Just X) = Pictures [rotate (-45) base, rotate 45 base]
        where base = line [(-1, 0), (1,0)]

