module PlayGrid
    ( drawGrid
    , screenToArea
    ) where

import Grid
import Graphics.Gloss
import Graphics.Gloss.Interface.Pure.Game
import Graphics.Gloss.Interface.IO.Game


drawGrid :: Drawable b => 
    (Int,Int) -> Color -> Int -> Grid b -> IO ()
drawGrid (x,y) col fps world = 
    play (InWindow "" (x,y) (0,0)) 
    col fps world 
    (draw $ screenToArea (x,y)) 
    (const id) (const id)

screenToArea :: (Int,Int) -> Area
screenToArea p = ((-x,-y), (x,y))
    where (u,v) = intPoint p
          (x,y) = (u/2, v/2)

