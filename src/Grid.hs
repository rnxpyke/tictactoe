{-# LANGUAGE FlexibleInstances, UndecidableInstances #-}

module Grid
    ( Area
    , Grid (..)
    , Drawable (..)
    , Circ (..)
    , getSize
    , getCenter
    , intPoint
    , screenToArrayCord
    ) where

import Data.Array
import Graphics.Gloss
import Graphics.Gloss.Data.Point
import qualified Graphics.Gloss.Data.Point.Arithmetic as P


--Grid assumes a typical 2d array, with starting bounds at (0,0)
data Grid a = Grid (Array (Int,Int) a) deriving (Eq, Show)
type Area = (Point,Point)

--area Functions
getSize :: Area -> Point
getSize ((x1,y1),(x2,y2)) = (max x1 x2 - min x1 x2, max y1 y2 - min y1 y2)

getCenter :: Area -> Point
getCenter area = (botLeft area) P.+ 0.5 P.* (getSize area)

transArea :: Area -> Point -> Area
transArea (x,y) p = (x P.+ p, y P.+ p) 

topLeft :: Area -> Point
topLeft ((a,b), (x,y)) = (min a x, max b y)

botLeft :: Area -> Point
botLeft ((a,b), (x,y)) = (min a x, min b y)

--additional point functions
mulPoint :: Point -> Point -> Point
mulPoint (a,b) (x,y) = (a * x, b * y)

intPoint :: (Int,Int) -> Point
intPoint (x,y) = (fromIntegral x, fromIntegral y)

reciPoint :: Point -> Point
reciPoint (x,y) = (1/x, 1/y)

--to easily implement drawable, just implement the norm function
--then draw just uses norm appropriately scaled 
class Drawable a where
    --draw at specified area
    draw :: Area -> a -> Picture
    draw = drawFromNorm norm

    -- Normalized drawing
    norm :: a -> Picture
    norm = draw ((-1,-1), (1,1))

--allows easy drawable implementation
drawFromNorm :: (a -> Picture) -> Area -> a -> Picture
drawFromNorm drawNorm area a = translate tx ty $ scale sx sy (drawNorm a)
    where   (sx, sy) = 0.5 P.* getSize area
            (tx, ty) = getCenter area

--for use in array

bound :: (Int,Int) -> Point
bound p = (1,1) P.+ (intPoint p)

fracAreaSize :: Area -> Point -> Point
fracAreaSize a p = mulPoint (reciPoint p) (getSize a)

fracArea :: Area -> (Int,Int) -> Area
fracArea area upper = ((a,b-y), (a+x,b))
    where (a,b) = topLeft area
          (x,y) = fracAreaSize area $ bound upper

 --translate In Array Cords 
transArrayArea :: Area -> Point -> Area
transArrayArea ((ax,ay),(bx,by)) (x,y) = ((ax+x, ay-y), (bx + x, by-y))

--todo: add inverse
getBoundArea :: Area -> (Int,Int) -> (Int,Int) -> Area
getBoundArea area upper index = transArrayArea cellArea transp
    where cellSize = fracAreaSize area $ bound upper
          cellArea = fracArea area upper
          transp   = mulPoint cellSize $ intPoint index 

--test area
--todo: remove
area = ((5,5),(10,10)) :: Area

--todo add instance declaration
--todo implement getArea correctly
instance Drawable b => Drawable (Grid b) where
    draw area (Grid g) = Pictures $ drawAt . getArea <$> ass
        where ass = assocs g
              upper = snd $ bounds g
              getArea :: ((Int,Int),b) -> (Area,b)
              getArea (p, b) = (getBoundArea area upper p, b) 
              drawAt (area, a) = draw area a


--TestData
data Circ = Circ

instance Drawable Circ where
    draw ((x1,y1),(x2,y2)) Circ = line 
        [ (x1,y1)
        , (x1,y2)
        , (x2,y2)
        , (x2,y1)
        , (x1,y1)
        ]  

head' :: [a] -> Maybe a
head' []  = Nothing
head' (x:_) = Just x

pointInArea :: Area -> Point -> Bool
pointInArea (a,b) p = pointInBox p a b 

screenToArrayCord :: Area -> (Int,Int) -> Point -> Maybe (Int,Int)
screenToArrayCord area bounds p = head' reslist
  where  
    indexes = range ((0,0), bounds) 
    areas :: [Area]
    areas   = getBoundArea area bounds <$> indexes
    ass     = zip ((flip pointInArea) p <$> areas) indexes
    reslist = map snd $ filter ((==True) . fst) ass

