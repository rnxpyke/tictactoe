module Game 
    ( Player (..)
    , Cell
    , Game (..)
    , step
    , startGame
    , checkGame 
    ) where 

import Data.Array
import Data.Maybe

data Player = X | O deriving (Eq, Show)
type Cell = Maybe Player
type Board = Array (Int,Int) Cell

data Game = Game
    { turn :: Player
    , board :: Board
    , running :: Bool
    } deriving (Eq, Show)

startGame :: Game
startGame = Game
    { turn = X
    , board = startBoard
    , running = True
    }


checkGame :: Game -> Game
checkGame g = if checkBoard $ board g
              then g {running = False}
              else g



checkBoard :: Board -> Bool
checkBoard b = won
    where
        ls = assocs b
        frow x = filter ((== x) . fst . fst)
        fcol x = filter ((== x) . snd . fst)
        fr x = (map snd . frow x)
        fc x = (map snd . fcol x)
        fs = [fr, fc] <*> [0..2]
        res = fs <*>  [ls]
        diags = (fmap (get b))  <$> [[(0,0), (1,1), (2,2)], [(0,2), (1,1), (2,0)]]
        bools = map checkLine (res ++ diags)
        won = or bools

checkLine :: [Cell] -> Bool
checkLine s = isJust a  && all (== a) s
    where a = head s


-- returns second argument, if running in first is set
run :: Game -> Game -> Game
run a b = if running a then b else a

--Places one stone at the given position if possible
step :: (Int, Int) -> Game -> Game
step index game = run game (apply new)
  where apply Nothing = game
        apply (Just a) = swapPlayers $ game {board = a}
        new = setStone index (turn game) (board game)


swapPlayers :: Game -> Game
swapPlayers x = do
     let t = turn x
     x {turn = other t}

other :: Player -> Player
other X = O
other _ = X

emptyCell :: Cell
emptyCell = Nothing

startBoard :: Board
startBoard = listArray r $ replicate 9 emptyCell
   where r = ((0,0), (2,2))

get :: Board -> (Int,Int) -> Cell
get b i = b ! i

set :: Board -> Cell -> (Int,Int) -> Board
set b c i = b // [(i,c)]

printC :: Cell -> String
printC Nothing = "#"
printC (Just x) = show x

printB :: Board -> String
printB x = res
   where
    board = elems x
    a = take 3 board
    b = take 3 $ drop 3 board
    c = take 3 $ drop 6 board
    res = concat
      $ (++"\n") . concat . (printC <$>)
      <$> [a,b,c]

setStone :: (Int, Int) -> Player -> Board -> Maybe Board
setStone i p b
    | isNothing (get b i) = Just $ set b (Just p) i
    | otherwise = Nothing
