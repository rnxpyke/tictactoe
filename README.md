# tictactoe

This is the well known game of tictactoe.
It's a two player Game where you place stones
on a 3 by 3 grid in turns.
Whoever has 3 stones in a row first wins, if
the board is full and nobody has won yet, it's a draw.
To reset the game you can press the Space bar,
to place a stone just click into the desired spot.
