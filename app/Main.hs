module Main where

import Game
import Render
import Grid
import PlayGrid
import Graphics.Gloss.Interface.Pure.Game

main :: IO ()
main = game 

(wi,hi) = (400,400)
area = screenToArea (wi,hi)

window = InWindow "" (wi,hi) (1000,1000)

game = play (InWindow "" (wi, hi) (0,0)) white 0
        startGame
        (render area)
        (ghandler)
        (const id)


idMaybe :: Maybe (a -> a) -> (a -> a)
idMaybe (Just x) = x
idMaybe Nothing = id 

ghandler :: Event -> Game -> Game
ghandler (EventKey (MouseButton LeftButton) Down _ p) = 
       checkGame . (idMaybe $ step <$> screenToArrayCord area (2,2) p) 
ghandler (EventKey (SpecialKey KeySpace) Down _ _) = const startGame
ghandler _ = id
